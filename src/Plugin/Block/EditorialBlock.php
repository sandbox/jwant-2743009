<?php

/**
 * @file
 *
 */

namespace Drupal\editorial_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an 'Editorial' block.
 *
 * @Block(
 *   id = "editorial_block",
 *   admin_label = @Translation("Editorial Block"),
 * )
 */
class EditorialBlock extends BlockBase {

//  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
//    parent::__construct($configuration, $plugin_id, $plugin_definition);
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
//    return new static(
//      $configuration,
//      $plugin_id,
//      $plugin_definition,
//      $container->get('entity_type.manager')
//    );
//  }

//  public function defaultConfiguration() {
//    return array(
//      'block_example_string' => $this->t('A default value. This block was created at %time', array('%time' => date('c'))),
//    );
//  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();

    // @ToDo: unset default title value.

    $form['#tree'] = TRUE;

    // The total number of items to display.
    $form['display_num'] = array(
      '#type' => 'number',
      '#title' => $this->t('Number of items to display'),
      '#description' => $this->t('Enter the total number of items to display.'),
      '#default_value' => isset($config['display_num']) ? $config['display_num'] : '',
      '#required' => TRUE,
      '#min' => 1,
    );

    // Manually selected items.
    $form['sticky'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => $this->t('Select specific content items.'),
      '#description' => $this->t('Start typing to autocomplete content title.'),
      '#prefix' => '<div id="sticky-fieldset-wrapper">',
      '#suffix' => '</div>',
    );
    $sticky_items_num = $form_state->get('sticky_items_num');
    if (!isset($sticky_items_num)) {
      $form_state->set('sticky_items_num', 1);
      $sticky_items_num = 1;
    }
    for ($i = 0; $i < $sticky_items_num; $i++) {
      $form['sticky']['sticky_item'][$i] = array(
        '#type' => 'entity_autocomplete',
        '#target_type' => 'node',
      );
    }
    $form['sticky']['actions']['add_another'] = array(
      '#type' => 'submit',
      '#value' => t('Add another'),
      '#submit' => array('\Drupal\editorial_blocks\Plugin\Block\EditorialBlock::addAnother'),
      '#ajax' => array(
        'callback' => '\Drupal\editorial_blocks\Plugin\Block\EditorialBlock::addAnotherCallback',
        'wrapper' => 'sticky-fieldset-wrapper',
      ),
      '#limit_validation_errors' => array(),
    );
    if ($sticky_items_num > 1) {
      $form['sticky']['actions']['remove_one'] = array(
        '#type' => 'submit',
        '#value' => t('Remove last one'),
        '#submit' => array('::removeCallback'),
        '#ajax' => array(
          'callback' => '::addAnotherCallback',
          'wrapper' => 'sticky-fieldset-wrapper',
        ),
        '#limit_validation_errors' => array(),
      );
    }

    $bundles = array();
    foreach (\Drupal::entityManager()->getStorage('node_type')->loadMultiple() as $type) {
      $bundles[$type->id()] = $type->label();
    }

    $form['bundles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Bundles'),
      '#description' => t('Select which bundles to display.'),
      '#default_value' => isset($config['bundles']) ? $config['bundles'] : '',
      '#options' => $bundles,
      '#required' => TRUE,
    );

    $form['status'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Publish Status'),
      '#default_value' => isset($config['status']) ? $config['status'] : 1,
      '#options' => array(
        0 => $this->t('Unpublished'),
        1 => $this->t('Published'),
      ),
//      '#description' => t(''),
      '#required' => TRUE,
    );

    $form['sort'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Create Sort Order'),
      '#default_value' => isset($config['sort']) ? $config['sort'] : 0,
      '#options' => array(
        0 => $this->t('Sort Ascending'),
        1 => $this->t('Sort Descending'),
      ),
      '#description' => t('How to sort the content on create date.'),
      '#required' => TRUE,
    );

    // @Todo: Select a view mode to display content.

    // @Todo: Why is this needed?
    $form_state->setCached(FALSE);

    return $form;
  }

  /**
   * Sticky items add another submit callback.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function addAnother(array &$form, FormStateInterface $form_state) {
    $sticky_items_num = $form_state->get('sticky_items_num');
    $add_button = $sticky_items_num + 1;
    $form_state->set('sticky_items_num', $add_button);
    $form_state->setRebuild();
  }

  public static function addAnotherCallback(array &$form, FormStateInterface $form_state) {
    $sticky_items_num = $form_state->get('sticky_items_num');
    return $form['settings']['sticky'];
  }

  public static function removeCallback(array &$form, FormStateInterface $form_state) {
    $sticky_items_num = $form_state->get('sticky_items_num');
    if ($sticky_items_num > 1) {
      $remove_button = $sticky_items_num - 1;
      $form_state->setValue('sticky_items_num', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('display_num', $form_state->getValue('display_num'));
    $this->setConfigurationValue('bundles', $form_state->getValue('bundles'));
    $this->setConfigurationValue('status', $form_state->getValue('status'));
    $this->setConfigurationValue('sort', $form_state->getValue('sort'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $display_num = $form_state->getValue('display_num');

    if (!is_numeric($display_num) && $display_num > 0) {
      $form_state->setErrorByName('display_num', t('Needs to be a positive integer.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

//    $config = $this->getConfiguration();

    return array(
      '#type' => 'markup',
      '#markup' => $this->t('The is a test.'),
    );
  }
}
